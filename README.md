# css-exercices

## Exercice 1 - Selecteurs (exercice1.html)

Commencer par créer un fichier css et le linker au html
1. Souligner les h1
2. Mettre le texte des h2 des sections en rouge
3. Mettre les éléments ayant la classe bold en gras
4. Changer la taille du texte de l'élément avec l'id second
5. Mettre les h2 de l'élément avec l'id second en italique
6. Mettre une bordure noire sur les élément avec la class myClass
7. Mettre la couleur de la bordure en vert pour les éléments de type article avec la classe myClass
8. Mettre une couleur de fond gris claire aux span
9. Mettre en majuscule les span à l'intérieur de p
10. Mettre une marge de 20px aux p directement à l'intérieur de section
11. Indenter de 15px les h1 et les h2 directement dans un élément avec l'id second
12. Mettre un padding de 10px aux paragraphes ayant la classe bold

## Exercice 2 - Modificateur CSS (exercice2.html)
Faire un ptit menu déroulant
1. Dans le projet css-exercice, créer un nouveau fichier exercice2.html et sa feuille de style css/exercice2.css
2. Dans le html, faire un ul avec 2-3 li à l'intérieur et faire que chacun de ces li contienne (en plus du texte) un ul avec 2-3 li
3. Dans le css, faire en sorte de cacher par défaut les ul dans les li, et en utilisant le modificateur approprié, faire que le ul du li s'affiche quand on la souris passe sur celui ci
Bonus : Faire en sorte d'avoir un sous-sous-menu et éventuellement un sous-sous-sous-menu et adapter le code css si besoin pour que ça marche pour chaque niveau à l'infini